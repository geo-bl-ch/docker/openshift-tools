FROM alpine:3.19

RUN apk update && \
    apk upgrade && \
    apk add bash bash-completion git helm gettext yq && \
    apk add --virtual .build build-base go krb5-dev gpgme-dev libassuan-dev && \
    cd /tmp && \
    git clone https://github.com/openshift/oc.git && \
    cd oc && \
    git checkout openshift-clients-4.12.0-202208031327 && \
    make oc && \
    cp oc /usr/local/bin/oc && \
    cp contrib/completions/bash/oc /usr/share/bash-completion/completions/ && \
    apk del .build && \
    rm -rf /tmp/*
