# openshift-tools

Alpine container with
- openshift-client
- helm
- git
- bash
- bash-completion
- gettext
- yq
